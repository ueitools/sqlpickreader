using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using PAL = PickAccessLayer;
using ToolsData;
using IRow = System.Collections.Generic.IDictionary<string, string>;
using BusinessObject;


namespace SQLPickReader
{
    public abstract class PickIdFields
    {
        public const string Sequence = "Sequence";
        public const string ID = "ID";
        public const string SourceID = "SourceID";
        public const string Flags = "Flags";
        public const string Version = "Version";
        public const string Time = "Time";
        public const string Executor_Code = "Executor_Code";
        public const string Name = "Name";
        public const string Text = "Text";
        public const string IsInversedData = "IsInversedData";
        public const string IsExternalPrefix = "IsExternalPrefix";
        public const string IsRestricted = "IsRestricted";
        public const string IsFrequencyData = "IsFrequencyData";
        public const string IsInversedPrefix = "IsInversedPrefix";
    }

    public abstract class PickItemFields
    {
        public const string ID = "ID";
        public const string DATA = "DATA";
        public const string Flags = "Flags";
        public const string Label = "Label";
        public const string Intron = "Intron";
        public const string Outron = "Outron";
        public const string Text = "Text";
    }

    public abstract class PrefixItemFields
    {
        public const string ID = "ID";
        public const string DATA = "DATA";
        public const string Text = "Text";
    }
    public class SQLPickReader:IPickReader
    {
        private string _pickDbName;
        PAL.Pick2DAO pickaccess;
        PickData.PickIdHeader idDetails = new PickData.PickIdHeader();

        private List<string> _idList;
        private string _id;
        public List<string> IdList
        {
            get { return _idList; }
            set
            {
                _idList = new List<string>();
                if (value != null)
                {
                    _idList = value;
                }
            }
        }


        public string PickDbName
        {
            get { return _pickDbName; }
            set { _pickDbName = value; }
        }
        public SQLPickReader()
        {
            pickaccess = new PickAccessLayer.Pick2DAO();
        }

        private bool GetIdSequence(string _idToSearch)
        {
            if (_idToSearch.CompareTo(_id) == 0)
                return true;
            else
                return false;
            
        }
      

        #region IPickReader Members

        public IRow GetPickID(string id, int version)
        {
            IDictionary<string, string> picklist = new Dictionary<string, string>();
            _id = id;
            //int idSequence = IdList.FindIndex(GetIdSequence);
            List<Hashtable> idInfo = (List<Hashtable>)PickAccessLayer.PickRetrieveFunctions.GetPickIdInfo(PickDbName, id, version);
            
            idDetails = PickAccessLayer.PickRetrieveFunctions.GetPickIdHeader(PickDbName, id, version);
            
                      //List<Hashtable> idInfo=(List<Hashtable>)pickaccess.GetPickIdInfo(PickDbName, id, version);
            IList<IRow> rowList=IRowHelper.GetRowList(idInfo);
                        
            if (rowList.Count > 0)
            {
                IRow row = rowList[0];

                return TransformID(row, ConvertFlagToString(idDetails.IsInversedPrefix));
                
            }
            else
                return null;

            
        }

        public IList<IDictionary<string, string>> GetPickItem(string id, int version)
        {
            IDictionary<string, string> picklist = new Dictionary<string, string>();
            _id = id;
            List<Hashtable> idInfo = new List<Hashtable>();
            idInfo = (List<Hashtable>)PickAccessLayer.PickRetrieveFunctions.GetPickItems_Test(PickDbName, _id, version);
            //idInfo = (List<Hashtable>)pickaccess.GetPickItems_Test(PickDbName, _id, version);
            IList<IRow> rowList = IRowHelper.GetRowList(idInfo);
            foreach (IRow row in rowList)
            {
                TransformData(row);
            }

            return rowList;
        }

        public IList<IDictionary<string, string>> GetPrefixItem(string id, int version)
        {
            IDictionary<string, string> picklist = new Dictionary<string, string>();
            _id = id;
            List<Hashtable> idInfo = new List<Hashtable>();
            idInfo = (List<Hashtable>)PickAccessLayer.PickRetrieveFunctions.GetPickPrefixItems_Test(PickDbName, id, version);
            //idInfo = (List<Hashtable>)pickaccess.GetPickPrefixItems_Test(PickDbName, id, version);
            IList<IRow> rowList = IRowHelper.GetRowList(idInfo);
            foreach (IRow row in rowList)
            {
                TransformData(row);
            }

            return rowList;
        }

        public List<int> ConvertPrefixList(IList<IDictionary<string, string>> PrefixList)
        {
            List<int> iPrefixList = new List<int>();
            foreach (Dictionary<string, string> item in PrefixList)
            {
                if (item["DATA"] != null && item["DATA"] != "")
                    iPrefixList.Add(Convert.ToInt32(item["DATA"], 2));
            }
            return iPrefixList;
        }

        public int GetMaxVersion(string id)
        {
            return PickAccessLayer.PickRetrieveFunctions.GetMaxVersion(PickDbName, id);

            //return pickaccess.GetMaxVersion(PickDbName, id); 
        }

        public IList<string> GetAllIDList()
        {
            IList<Hashtable> datalist = PickAccessLayer.PickRetrieveFunctions.GetPickIdList(PickDbName);
            //IList<Hashtable> datalist = pickaccess.GetPickIdList(PickDbName);
            IList<string> idlist = new List<string>();
            foreach (Hashtable data in datalist)
            {
                idlist.Add(data["FK_ID"].ToString());
            }
            IdList =(List<string>) idlist;
            return idlist;
        }

        public ToolsData.Id.List GetAllIDList(ToolsData.IdFilterConditions idFilterConditions)
        {
            switch (idFilterConditions.FilterType)
            {
                case IdFilterConditions.FilterByType.Mode:
                    return GetAllIDList_Modes(idFilterConditions.FilterModes);
                case IdFilterConditions.FilterByType.Range:
                    return GetAllIDList_Range(idFilterConditions.FilterRange);
                case IdFilterConditions.FilterByType.File:
                    return idFilterConditions.FilterIdsFromFile;
                default:
                    IList<string> idList = ((IPickReader)this).GetAllIDList();
                    return new Id.List(idList);
            }
        }

        #endregion

        private Id.List GetAllIDList_Modes(List<char> modes)
        {
            IList<string> allIDList = ((IPickReader)this).GetAllIDList();

            Id.List idList = new Id.List();
            foreach (string id in allIDList)
            {
                foreach (char mode in modes)
                {
                    if (id.ToUpper()[0] == char.ToUpper(mode))
                    {
                        idList.Add(new Id(id));
                    }
                }
            }
            return idList;
        }

        private Id.List GetAllIDList_Range(IdFilterConditions.IdFilterRange filterRange)
        {
            IList<string> allIDList = ((IPickReader)this).GetAllIDList();

            Id.List idList = new Id.List();
            foreach (string id in allIDList)
            {
                Id pickId = new Id(id);
                if (filterRange.IsInRange(pickId))
                {
                    idList.Add(pickId);
                }
            }
            return idList;
        }

        string IsInversedPrefix(string id, int version)
        {
            return PickAccessLayer.PickRetrieveFunctions.GetInversePrefixFlag(PickDbName, id, version);
        }

        /// <summary>
        /// parsing
        /// 1. replace flags with IsInversedData, IsExternalPrefix, 
        ///    IsRestricted, IsFrequencyData
        /// 2. add IsInversedPrefix
        /// 3. time to datetime
        /// 4. id to fixed 4 digit format
        /// </summary>
        /// <param name="row"></param>
        /// <param name="isInversedPrefix"></param>
        /// <returns></returns>
       
        IDictionary<string, string> TransformID(
            IRow row, string isInversedPrefix)
        {
            string strFlags = row["Flags"];
            
            //IRow flagRow = LegacyParser.ParseIDFlag(LegacyParser.StringFlagToInt(strFlags));
            
            //row.Add("IsInversedData", flagRow["IsInversedData"]);
            row.Add("IsInversedData", ConvertFlagToString(idDetails.IsInversedData));
            //row.Add("IsExternalPrefix", flagRow["IsExternalPrefix"]);
            row.Add("IsExternalPrefix", ConvertFlagToString(idDetails.IsExternalPrefix));
            //row.Add("IsRestricted", flagRow["IsRestricted"]);
            row.Add("IsRestricted", ConvertFlagToString(idDetails.IsRestricted));
            //row.Add("IsFrequencyData", flagRow["IsFrequencyData"]);
            row.Add("IsFrequencyData", ConvertFlagToString(idDetails.IsFrequencyData));
            //row.Add("IsInversedPrefix", isInversedPrefix);
            row.Add("IsInversedPrefix", ConvertFlagToString(idDetails.IsInversedPrefix));

            //uint unixtime = UInt32.Parse(row["Time"]);
            //row["Time"] = row["Time"];
            row["ID"] = LegacyParser.FromLegacyIDFormat(row["ID"]);
            row.Remove("Flags");

            return row;
        }

        void TransformData(IRow row)
        {
            if (!string.IsNullOrEmpty(row["DB"]))
            {
                //Start Modify
                //Old code
                //string data = LegacyParser.ParseData(
                //    row["DB"], row["BitCount"]);
                //New code
                string data = (string)row["DB"];
                //End Modify

                row.Add("DATA", data);
                row["ID"] = LegacyParser.FromLegacyIDFormat(row["ID"]);

                row.Remove("DB");
                row.Remove("BitCount");
            }
            else
            {
                row.Add("DATA", "");
                row["ID"] = LegacyParser.FromLegacyIDFormat(row["ID"]);
                row.Remove("DB");
                row.Remove("BitCount");
            }
        }

        private string ConvertFlagToString(bool statusFlag)
        {
            if (statusFlag == true)
                return "Y";
            else
                return "N";
        }
    }
}
